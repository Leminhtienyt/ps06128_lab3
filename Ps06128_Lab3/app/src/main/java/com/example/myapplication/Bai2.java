package com.example.myapplication;

import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.widget.Button;
import android.widget.Toast;

public class Bai2 extends AppCompatActivity {
    Button btn2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai2);
        btn2=(Button) findViewById(R.id.btnBai2);
        btn2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                accessTheCallLog();
                //finish();
            }
        });
    }
    public void accessTheCallLog()
    {
        String [] projection=new String[]{
                Calls.DATE,
                Calls.NUMBER,
                Calls.DURATION
        };
        Cursor c=getContentResolver().query(
                CallLog.Calls.CONTENT_URI,
                projection,
                Calls.DURATION+"<?",new String[]{"30"},
                Calls.DATE +" Asc");
        c.moveToFirst();
        String s="";
        while(c.isAfterLast()==false){
            for(int i=0;i<c.getColumnCount();i++){
                s+=c.getString(i)+" - ";
            }
            s+="\n";
            c.moveToNext();
        }
        c.close();
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }
}
