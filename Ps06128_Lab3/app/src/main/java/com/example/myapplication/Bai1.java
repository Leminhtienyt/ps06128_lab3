package com.example.myapplication;


import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;




import androidx.appcompat.app.AppCompatActivity;

import android.view.View.OnClickListener;
import android.provider.ContactsContract;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import android.view.View;
import java.util.ArrayList;


public class Bai1 extends AppCompatActivity {
    Button btn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_bai1);
        btn1=(Button) findViewById(R.id.btnBai1);
        btn1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showAllContacts();
                //finish();
            }
        });
    }
    public void showAllContacts() {
        Uri uri = Uri.parse("content://contacts/people");
        ArrayList<String> list = new ArrayList<String>();
        Cursor c1 = getContentResolver()
                .query(uri, null, null, null, null);
        c1.moveToFirst();
        while (c1.isAfterLast() == false) {
            String s = "";
            String idColumnName = ContactsContract.Contacts._ID;
            int idIndex = c1.getColumnIndex(idColumnName);
            s = c1.getString(idIndex) + " - ";
            String nameColumnName = ContactsContract.Contacts.DISPLAY_NAME;
            int nameIndex = c1.getColumnIndex(nameColumnName);
            s += c1.getString(nameIndex);
            c1.moveToNext();
            list.add(s);
        }
        c1.close();
        ListView lv = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, list);
        lv.setAdapter(adapter);
    }
}

